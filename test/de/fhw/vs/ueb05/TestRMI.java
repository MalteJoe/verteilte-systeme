package de.fhw.vs.ueb05;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.junit.Test;

public class TestRMI {
    private static DataBase readStubFromFile(final String fileName)
            throws FileNotFoundException, IOException, ClassNotFoundException {
        /* Deserialize stub using Java Serialization */
        final FileInputStream fis = new FileInputStream(fileName);
        final ObjectInputStream in = new ObjectInputStream(fis);
        final DataBase remoteObj = (DataBase) in.readObject();
        in.close();
        return remoteObj;
    }

    @Test
    public final void test() throws FileNotFoundException, IOException, ClassNotFoundException {
        final DataBaseClient client = new DataBaseClient(readStubFromFile(DataBaseImpl.STUB_FILE));
        client.addRecord(4101, "Appen");
        client.addRecord(4102, "Ahrensburg");
        client.addRecord(4103, "Wedel");
        client.addRecord(4104, "Aumühle");
        client.addRecord(4105, "Seevetal");
        client.addRecord(4106, "Quickborn");

        assertEquals("Wedel", client.getRecord(4103));
        assertNull(client.getRecord(4107));
        assertEquals(6, client.getSize());
    }

}
