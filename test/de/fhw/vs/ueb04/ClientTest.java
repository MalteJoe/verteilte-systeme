package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fhw.vs.ueb04.LogMessageProtos.LogMessage;
import de.fhw.vs.ueb04.LogMessageProtos.LogMessage.Urgency;

public class ClientTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public final void test() throws UnknownHostException, IOException {
        final LogMessage msg = LogMessage.newBuilder().setTimestamp(System.currentTimeMillis())
                .setUrgency(Urgency.DEBUG).setContent("Erste Test-Nachricht").build();
        ProtocolBufferClient.sendMessage("localhost", msg);
    }

}
