package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DBTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public final void test() throws UnknownHostException, IOException {
        DBClient.addRecord(4101, "Appen");
        DBClient.addRecord(4102, "Ahrensburg");
        DBClient.addRecord(4103, "Wedel");
        DBClient.addRecord(4104, "Aumühle");
        DBClient.addRecord(4105, "Seevetal");
        DBClient.addRecord(4106, "Quickborn");

        Assert.assertEquals("Wedel", DBClient.getRecord(4103));
        Assert.assertNull(DBClient.getRecord(4107));
        Assert.assertEquals(6, DBClient.getSize());
    }

}
