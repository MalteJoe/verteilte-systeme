package de.fhw.vs.ueb02;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SocketClientTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public final void testHttpRequestExtern() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequest("www.tagesschau.de", "/");
        Assert.assertTrue(response.contains("200 OK"));
        System.out.println(response);
    }

    @Test
    public final void testHttpRequestExternProxy() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequestProxy("www.tagesschau.de", "/");
        Assert.assertTrue(response.contains("200 OK"));
        System.out.println(response);
    }

    @Test
    public final void testHttpRequestIntern() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequest("www.fh-wedel.de", "/");
        Assert.assertTrue(response.contains("200 OK"));
        System.out.println(response);
    }

    @Test
    public final void testHttpRequestInternProxy() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequestProxy("www.fh-wedel.de", "/");
        Assert.assertTrue(response.contains("200 OK"));
        System.out.println(response);
    }

    @Test
    public final void testURLRequestExtern() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequestURL("www.tagesschau.de", "/");
        Assert.assertTrue(response.contains("html"));
        System.out.println(response);
    }

    @Test
    public final void testURLRequestIntern() throws UnknownHostException, IOException {
        final String response = SocketClient.httpRequestURL("www.fh-wedel.de", "/");
        Assert.assertTrue(response.contains("html"));
        System.out.println(response);
    }

}
