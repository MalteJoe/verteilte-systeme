package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import de.fhw.vs.ueb04.LogMessageProtos.LogMessage;

public class ProtocolBufferClient {
    /** Standardport */
    private static final int DEFAULT_PORT = 1337;

    public static void sendMessage(final String host, final LogMessage msg) throws UnknownHostException, IOException {

        final Socket s = new Socket(host, DEFAULT_PORT);
        msg.writeTo(s.getOutputStream());
    }
}
