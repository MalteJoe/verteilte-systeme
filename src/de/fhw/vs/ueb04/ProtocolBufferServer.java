package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.fhw.vs.ueb04.LogMessageProtos.LogMessage;

class ClientHandler implements Runnable {
    private static final SimpleDateFormat sdf = new SimpleDateFormat();

    private final Socket                  conn;

    ClientHandler(final Socket conn) {
        this.conn = conn;
    }

    @Override
    public void run() {

        try {
            final LogMessage msg = LogMessage.parseFrom(this.conn.getInputStream());
            final Calendar timestamp = Calendar.getInstance();
            timestamp.setTimeInMillis((msg.getTimestamp()));
            System.out.print(
                    sdf.format(timestamp.getTime()) + " " + msg.getUrgency().toString() + " - " + msg.getContent());
            if (msg.hasId()) {
                System.out.print(" by " + msg.getId().getCreator() + ", origin: " + msg.getId().getOrigin());
            }
            System.out.println();
        }

        catch (final IOException e) {
            System.err.println("IOException on socket : " + e);
            e.printStackTrace();
        }
    }

}

public class ProtocolBufferServer {

    public static void echo(final String msg) {
        System.out.println(msg);
    }

    public static void main(final String args[]) {
        ServerSocket s = null;
        Socket conn = null;

        try {
            s = new ServerSocket(1337);

            // 2. Wait for an incoming connection
            echo("Server socket created.Waiting for connection...");

            while (true) {
                // get the connection socket
                conn = s.accept();

                // print the hostname and port number of the connection
                echo("Connection received from " + conn.getInetAddress().getHostName() + " : " + conn.getPort());

                // create new thread to handle client
                new Thread(new ClientHandler(conn)).start();
            }
        }

        catch (final IOException e) {
            System.err.println("IOException");
        }

        // 5. close the connections and stream
        try {
            s.close();
        }

        catch (final IOException ioException) {
            System.err.println("Unable to close. IOexception");
        }
    }
}
