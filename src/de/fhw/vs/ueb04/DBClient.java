package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import de.fhw.vs.ueb04.DbRpcProtos.RPCRequest;
import de.fhw.vs.ueb04.DbRpcProtos.RPCRequest.AddRecord;
import de.fhw.vs.ueb04.DbRpcProtos.RPCRequest.Operation;
import de.fhw.vs.ueb04.DbRpcProtos.RPCResponse;

public class DBClient {
    /** Standardport */
    private static final int DEFAULT_PORT = 1337;

    /**
     * Fügt der Datenbank einen Eintrag unter einer ID hinzu
     * 
     * @param id ID des Eintrags
     * @param value Inhalt des Eintrags
     * @throws UnknownHostException
     * @throws IOException
     */
    public static void addRecord(final int id, final String value) throws UnknownHostException, IOException {
        doRequest(RPCRequest.newBuilder().setOp(Operation.ADD_RECORD)
                .setAddRecord(AddRecord.newBuilder().setId(id).setRecord(value).build()).build());
    }

    /**
     * Führt den eigentlichen RPC-Request aus
     * 
     * @param request RPC-Request
     * @return RPC-Response
     * @throws UnknownHostException
     * @throws IOException
     */
    private static RPCResponse doRequest(final RPCRequest request) throws UnknownHostException, IOException {

        final Socket s = new Socket("localhost", DEFAULT_PORT);
        System.out.println("Sending message:\n" + request);
        request.writeDelimitedTo(s.getOutputStream());
        final RPCResponse response = RPCResponse.parseDelimitedFrom(s.getInputStream());
        System.out.println("Got Response:\n" + response);
        s.close();
        return response;
    }

    /**
     * Liefert einen Eintrag von dem Datenbankserver
     * 
     * @param i ID des gesuchten Eintrags
     * @return Inhalt des Eintrags oder <tt>null</tt>, wenn der Eintrag nicht vorhanden ist
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String getRecord(final int i) throws UnknownHostException, IOException {
        final RPCResponse response =
                doRequest(RPCRequest.newBuilder().setOp(Operation.GET_RECORD).setGetRecord(i).build());
        return (response.getOp() == RPCResponse.Operation.GET_RECORD) ? response.getGetRecord() : null;

    }

    /**
     * Liefert die aktuelle Größe der Datenbank
     * 
     * @return Anzahl der Datensätze in der Datenbank
     * @throws UnknownHostException
     * @throws IOException
     */
    public static int getSize() throws UnknownHostException, IOException {
        return doRequest(RPCRequest.newBuilder().setOp(Operation.GET_SIZE).build()).getGetSize();
    }
}
