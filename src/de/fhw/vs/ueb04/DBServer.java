package de.fhw.vs.ueb04;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.fhw.vs.ueb04.DbRpcProtos.RPCRequest;
import de.fhw.vs.ueb04.DbRpcProtos.RPCResponse;
import de.fhw.vs.ueb04.DbRpcProtos.RPCResponse.Builder;
import de.fhw.vs.ueb04.DbRpcProtos.RPCResponse.Operation;

/**
 * Bearbeitung einer Client-Anfrage
 * 
 * @author malte
 */
class DBClientHandler implements Runnable {
    /** Socket, über den die Anfrage angenommen wurde */
    private final Socket conn;

    /**
     * Konstruktor
     * 
     * @param conn Socket, über den die Anfrage angenommen wurde
     */
    DBClientHandler(final Socket conn) {
        this.conn = conn;
    }

    @Override
    public void run() {

        try {
            final RPCRequest request = RPCRequest.parseDelimitedFrom(this.conn.getInputStream());
            System.out.println("Got request:\n" + request);
            final Builder responseBuilder = RPCResponse.newBuilder();
            switch (request.getOp()) {
            case ADD_RECORD:
                DBServer.addRecord(request.getAddRecord().getId(), request.getAddRecord().getRecord());
                responseBuilder.setOp(Operation.ADD_RECORD);
                break;
            case GET_RECORD:
                final String requestedRecord = DBServer.getRecord(request.getGetRecord());
                if (requestedRecord != null) {
                    responseBuilder.setOp(Operation.GET_RECORD).setGetRecord(requestedRecord);
                } else {
                    responseBuilder.setOp(Operation.ERROR)
                            .setErrorMsg("ID " + request.getGetRecord() + " not in database!");
                }
                break;
            case GET_SIZE:
                responseBuilder.setOp(Operation.GET_SIZE).setGetSize(DBServer.getSize());
                break;
            }
            final RPCResponse response = responseBuilder.build();
            System.out.println("Sending response:\n" + response);
            response.writeDelimitedTo(this.conn.getOutputStream());
        }

        catch (final IOException e) {
            System.err.println("IOException on socket : " + e);
            e.printStackTrace();
        }
    }

}

public class DBServer {
    /** Datenbank als einfache Map ID -> Wert */
    private static Map<Integer, String> database = Collections.synchronizedMap(new HashMap<>());

    /**
     * Fügt der Datenbank einen Eintrag hinzu
     * 
     * @param parseInt ID unter der der Eintrag gespeichert wird
     * @param textContent Inhalt
     * @return Inhalt, der zuvor mit der ID assoziiert war, sonst <tt>null</tt>
     */
    public static String addRecord(final int parseInt, final String textContent) {
        return database.put(parseInt, textContent);
    }

    /**
     * Liefert einen Eintrag aus der Datenbank
     * 
     * @param parseInt ID des gesuchten Eintrags
     * @return Eintrag oder <tt>null</tt>, falls kein Eintrag vorhanden
     */
    public static String getRecord(final int parseInt) {
        return database.get(parseInt);

    }

    /**
     * @return die Anzahl der aktuell gespeicherten Datensätze
     */
    public static int getSize() {
        return database.size();
    }

    public static void main(final String args[]) {
        ServerSocket s = null;
        Socket conn = null;

        try {
            s = new ServerSocket(1337);

            // 2. Wait for an incoming connection
            System.out.println("Server socket created.\nWaiting for connection...");

            while (true) {
                // get the connection socket
                conn = s.accept();

                // print the hostname and port number of the connection
                System.out.println(
                        "Connection received from " + conn.getInetAddress().getHostName() + " : " + conn.getPort());

                // create new thread to handle client
                new Thread(new DBClientHandler(conn)).start();
            }
        }

        catch (final IOException e) {
            System.err.println("IOException");
        }

        // 5. close the connections and stream
        try {
            s.close();
        }

        catch (final IOException ioException) {
            System.err.println("Unable to close. IOexception");
        }
    }
}
