package de.fhw.vs.ueb01;

/**
 * Verwenden Sie die synchronisierende Fassung von Counter und versuchen bitte erneut
 * einen Fehler zu erhalten.
 * 
 * @author malte
 */
public class Main4 {

    private static final int THREAD_COUNT = 4;

    public static void main(final String[] args) {

        final Counter c = new SynchronizedCounter();

        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(() -> {
                while (true) {
                    if (Math.floor(Math.random() * (THREAD_COUNT + 1)) % THREAD_COUNT == 0) {
                        c.decrement();
                    } else {
                        c.increment();
                    }
                }

            }).start();
        }
    }

}
