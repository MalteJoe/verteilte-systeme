package de.fhw.vs.ueb01;

/**
 * Definieren Sie bitte Threads, die die Counter–Klasse verwenden und feststellen, ob eine
 * Thread–Interferenz aufgetreten ist. Versuchen sie einen Interferenz–Fehler zu provozieren.
 *
 * @author malte
 */
public class Main3 {

    private static final int THREAD_COUNT = 20;

    public static void main(final String[] args) {

        final Counter c = new Counter();

        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(() -> {
                while (true) {
                    if (Math.floor(Math.random() * (THREAD_COUNT + 1)) % THREAD_COUNT == 0) {
                        c.decrement();
                    } else {
                        c.increment();
                    }
                }

            }).start();
        }
    }

}
