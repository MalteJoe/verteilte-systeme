package de.fhw.vs.ueb01;

/**
 * Schreiben Sie bitte ein einfaches Programm, das zwei oder mehr Threads erzeugt und
 * startet. Jeder Thread produziert Ausgaben. Lassen Sie bitte das Programm laufen und
 * beobachten Sie sein Ausgabeverhalten.
 *
 * @author malte
 */
public class Main2 {

    private static final int THREAD_COUNT = 4;

    public static void main(final String[] args) {
        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(() -> {
                while (true) {
                    System.out.println(Thread.currentThread().getName());
                }
            }).start();
        }
    }

}
