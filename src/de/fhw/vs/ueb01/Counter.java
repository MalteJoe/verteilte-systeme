package de.fhw.vs.ueb01;

class Counter {
    private int c = 0;

    public void decrement() {
        int d = this.c;
        final int before = d;
        d = d - 1;
        this.c = d;
        assert before - 1 == this.c : "decrement from " + before + " resulted in " + this.c;
    }

    public void increment() {
        int b = this.c;
        final int before = b;
        b = b + 1;
        this.c = b;
        assert before + 1 == this.c : "increment from " + before + " resulted in " + this.c;
    }

    public int value() {
        return this.c;
    }
}
