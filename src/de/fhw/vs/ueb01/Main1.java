package de.fhw.vs.ueb01;

import java.util.stream.Stream;

/**
 * Legen Sie bitte ein neues Eclipse-Projekt an (Java-Applikation) und definieren Sie dort
 * bitte ein Hallo-Welt-Programm, das zusätzlich einen Kommandozeilen-Parameter in args
 * erwartet und ausgibt. Definieren Sie bitte eine Start-Konfiguration für Ihr Projekt in dem
 * sie auch einen Kommandozeilen-Parameter festlegen.
 *
 * @author malte
 */
public class Main1 {

    public static void main(final String[] args) {
        Stream.of(args).forEach(System.out::println);
    }

}
