package de.fhw.vs.ueb01;

class SynchronizedCounter extends Counter {

    @Override
    public synchronized void decrement() {
        super.decrement();
    }

    @Override
    public synchronized void increment() {
        super.increment();
    }

    @Override
    public synchronized int value() {
        return super.value();
    }
}
