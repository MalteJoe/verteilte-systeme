package de.fhw.vs.ueb05;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DataBaseImpl implements DataBase {

    static final String                 STUB_FILE = "/tmp/RMI.stub";
    static final int                    PORT      = 1337;

    /** Datenbank als einfache Map ID -> Wert */
    private static Map<Integer, String> database  = Collections.synchronizedMap(new HashMap<>());

    public static void main(final String[] args) throws FileNotFoundException, IOException {
        new File(DataBaseImpl.STUB_FILE).getParentFile().mkdirs();
        final DataBase db = new DataBaseImpl();
        final Remote stub = UnicastRemoteObject.exportObject(db, PORT);
        writeStubToFile(STUB_FILE, stub);
        System.out.println("Server ready.");
    }

    private static void writeStubToFile(final String fileName, final Remote stub)
            throws FileNotFoundException, IOException {
        final FileOutputStream fos = new FileOutputStream(fileName);
        final ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(stub);
        out.close();
    }

    @Override
    public void addRecord(final int index, final String record) throws RemoteException {
        database.put(index, record);

    }

    @Override
    public String getRecord(final int index) throws RemoteException {
        return database.getOrDefault(index, null);
    }

    @Override
    public int getSize() throws RemoteException {
        return database.size();
    }

    @Override
    public String toString() {
        return database.toString();
    }

}
