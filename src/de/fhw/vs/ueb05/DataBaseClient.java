package de.fhw.vs.ueb05;

import java.io.IOException;
import java.net.UnknownHostException;

public class DataBaseClient {
    private final DataBase db;

    public DataBaseClient(final DataBase db) {
        this.db = db;
    }

    /**
     * Fügt der Datenbank einen Eintrag unter einer ID hinzu
     *
     * @param id ID des Eintrags
     * @param value Inhalt des Eintrags
     * @throws UnknownHostException
     * @throws IOException
     */
    public void addRecord(final int id, final String value) throws UnknownHostException, IOException {
        this.db.addRecord(id, value);
    }

    /**
     * Liefert einen Eintrag von dem Datenbankserver
     *
     * @param i ID des gesuchten Eintrags
     * @return Inhalt des Eintrags oder <tt>null</tt>, wenn der Eintrag nicht vorhanden ist
     * @throws UnknownHostException
     * @throws IOException
     */
    public String getRecord(final int i) throws UnknownHostException, IOException {
        return this.db.getRecord(i);

    }

    /**
     * Liefert die aktuelle Größe der Datenbank
     *
     * @return Anzahl der Datensätze in der Datenbank
     * @throws UnknownHostException
     * @throws IOException
     */
    public int getSize() throws UnknownHostException, IOException {
        return this.db.getSize();
    }
}
