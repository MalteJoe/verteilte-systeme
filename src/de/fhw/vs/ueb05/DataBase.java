package de.fhw.vs.ueb05;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DataBase extends Remote {
    public void addRecord(int index, String record) throws RemoteException;

    public String getRecord(int index) throws RemoteException;

    public int getSize() throws RemoteException;
}
