package de.fhw.vs.ueb03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

class ClientHandler implements Runnable {
    private final Socket conn;

    ClientHandler(final Socket conn) {
        this.conn = conn;
    }

    private void respond(final PrintStream out, final String record) {
        out.println(record);

    }

    @Override
    public void run() {
        String line;

        try {
            // get socket writing and reading streams
            final BufferedReader in = new BufferedReader(new InputStreamReader(this.conn.getInputStream()));
            final PrintStream out = new PrintStream(this.conn.getOutputStream());
            // Now start reading input from client
            while ((line = in.readLine()) != null && !line.isEmpty()) {
                // Overread non-empty lines
            }
            final Document doc =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.conn.getInputStream());
            switch (doc.getElementsByTagName("methodName").item(0).getTextContent()) {
            case "getRecord":
                respond(out, DatabaseServer
                        .getRecord(Integer.parseInt(doc.getElementsByTagName("int").item(0).getTextContent())));
                break;
            case "addRecord":
                respond(out,
                        DatabaseServer.addRecord(
                                Integer.parseInt(doc.getElementsByTagName("int").item(0).getTextContent()),
                                doc.getElementsByTagName("string").item(0).getTextContent()));
                break;
            case "getSize":
                respond(out, DatabaseServer.getSize());
            }

            // client disconnected, so close socket
            this.conn.close();
        }

        catch (final IOException | SAXException | ParserConfigurationException e) {
            System.out.println("IOException on socket : " + e);
            e.printStackTrace();
        }
    }

}
