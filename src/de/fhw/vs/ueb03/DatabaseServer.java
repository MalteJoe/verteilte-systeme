package de.fhw.vs.ueb03;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DatabaseServer {

    private static Map<Integer, String> database = Collections.synchronizedMap(new HashMap<>());

    public static String addRecord(final int parseInt, final String textContent) {
        return database.put(parseInt, textContent);
    }

    public static void echo(final String msg) {
        System.out.println(msg);
    }

    public static String getRecord(final int parseInt) {
        return database.get(parseInt);

    }

    public static String getSize() {
        // TODO Auto-generated method stub
        return Integer.toString(database.size());
    }

    public static void main(final String args[]) {
        ServerSocket s = null;
        Socket conn = null;

        try {
            // 1. creating a server socket - 1st parameter is port number and 2nd is the backlog
            s = new ServerSocket(5000, 10);

            // 2. Wait for an incoming connection
            echo("Server socket created.Waiting for connection...");

            while (true) {
                // get the connection socket
                conn = s.accept();

                // print the hostname and port number of the connection
                echo("Connection received from " + conn.getInetAddress().getHostName() + " : " + conn.getPort());

                // create new thread to handle client
                new Thread(new ClientHandler(conn)).start();
            }
        }

        catch (final IOException e) {
            System.err.println("IOException");
        }

        // 5. close the connections and stream
        try {
            s.close();
        }

        catch (final IOException ioException) {
            System.err.println("Unable to close. IOexception");
        }
    }
}
