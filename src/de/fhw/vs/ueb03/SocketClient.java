package de.fhw.vs.ueb03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

public class SocketClient {

    private static final InetSocketAddress FH_PROXY          = new InetSocketAddress("proxy.fh-wedel.de", 3128);
    private static final String            DEFAULT_PROTOCOL  = "http";
    /** Standardport */
    private static final int               HTTP_DEFAULT_PORT = 80;

    /**
     * TODO
     *
     * @param server
     * @param document
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String httpRequest(final String server, final String document)
            throws UnknownHostException, IOException {
        return httpRequest(server, document, HTTP_DEFAULT_PORT);
    }

    /**
     * TODO
     *
     * @param server
     * @param document
     * @param port
     * @return
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String httpRequest(final String server, final String document, final int port)
            throws UnknownHostException, IOException {
        final Socket s = new Socket(server, port);

        s.getOutputStream().write(("GET " + document + " HTTP/1.1\nHost: " + server + "\n\n").getBytes());
        return outputToString(s.getInputStream());
    }

    /**
     * TODO
     *
     * @param server
     * @param document
     * @param port
     * @return
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String httpRequestProxy(final String server, final String document)
            throws UnknownHostException, IOException {
        return httpRequestProxy(server, document, HTTP_DEFAULT_PORT);
    }

    /**
     * TODO
     *
     * @param server
     * @param document
     * @param port
     * @return
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String httpRequestProxy(final String server, final String document, final int port)
            throws UnknownHostException, IOException {
        final Socket s = new Socket(new Proxy(Type.HTTP, FH_PROXY));
        s.connect(new InetSocketAddress(server, port));

        s.getOutputStream().write(("GET http://" + server + document).getBytes());
        return outputToString(s.getInputStream());
    }

    public static String httpRequestURL(final String server, final String document)
            throws UnknownHostException, IOException {
        return httpRequestURL(server, document, DEFAULT_PROTOCOL);
    }

    /**
     * TODO
     *
     * @param server
     * @param document
     * @param port
     * @return
     * @throws UnknownHostException
     * @throws IOException
     */
    public static String httpRequestURL(final String server, final String document, final String protocol)
            throws UnknownHostException, IOException {
        final URL url = new URL(protocol, server, document);
        return outputToString(url.openStream());
    }

    /**
     * TODO
     *
     * @param in
     * @return
     * @throws IOException
     */
    private static String outputToString(final InputStream in) throws IOException {
        final BufferedReader input = new BufferedReader(new InputStreamReader(in));
        final StringBuilder output = new StringBuilder();
        String line;
        while ((line = input.readLine()) != null) {
            output.append(line).append("\n");
        }
        in.close();
        return output.toString();
    }

}
