-module(clock).
-export([start/1, get/1, run/2]).

run(Clock, TickTime) ->
    receive
        {set, Value} -> run(Value, TickTime);
        {get, Pid} -> Pid ! {clock, Clock},
                      run(Clock, TickTime);
        stop -> exit(normal)
        
    after TickTime -> run(Clock + 1, TickTime)
    end.
    
start(TickTime) ->
    spawn(?MODULE, run, [0, TickTime]).

get(Pid) ->
    Pid ! {get, self()},
    receive
        {clock, Clock} -> io:format("Clock is ~p~n", [Clock])
    end.
