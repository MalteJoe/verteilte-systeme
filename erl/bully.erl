-module(bully).
-export([loop/2, rpc/2]).

loop(Coordinator, Processlist) ->
    receive
        {Pid, election} -> sendCoordinatorToLower(Processlist), loop(Coordinator, Processlist);
        {Pid, coordinator} -> if Pid > self() ->
                                    loop(Pid, ProcessList);
                                true ->
                                    loop(Coordinator, ProcessList);
    end.
    
rpc(Pid, Request) ->
    Pid ! {self(), Request},
    receive
       {Pid, Response} ->
           Response
       after 250 ->
           unreachable
    end.

makeElection(ProcessList) ->
    Responses = lists:map(fun(Pid) -> rpc(Pid, election) end,
            lists:filter(fun(Pid) -> self() > Pid end, ProcessList)).
    lists:foreach(fun(Pid) -> receive
                                {Pid, }
            
sendCoordinatorToLower(ProcessList) ->
    lists:map(fun(Pid) -> rpc(Pid, coordinator) end,
            lists:filter(fun(Pid) -> Pid > self() end, ProcessList)).
    
