-module(ueb6).
-export([convert/2, maxitem/1, printmaxitem/1, diff/3]).

-define(INCH_TO_CM, 2.54).

convert(Amount, cm) ->
    {inch, Amount / ?INCH_TO_CM};
convert(Amount, inch) ->
    {cm, Amount * ?INCH_TO_CM}.

% > ueb6:convert(30, miles).
% ** exception error: no function clause matching ueb6:convert(30,miles) (ueb6.erl, line 6)

maxitem([]) -> 0;
maxitem([H|[]]) -> H;
maxitem([H|T]) -> max(H, maxitem(T)).

printmaxitem([]) ->
    io:format("maxitem([]) -> 0~n"),
    0;
printmaxitem([H|[]]) ->
    io:format("maxitem([~p]) -> ~p~n", [H, H]),
    H;
printmaxitem([H|T]) ->
    io:format("maxitem([~p, ~p])~n", [H, T]),
    Rest = printmaxitem(T),
    Maximum = max(H, Rest),
    io:format("max(~p, ~p) -> ~p~n", [H, Rest, Maximum]),
    Maximum.

diff(F, X, H) ->
    (F(X + H) - F(X - H)) / (2 * H).
% > ueb6:diff(fun(X) -> 2*X*X*X - 12*X + 3 end, 3, 1.0e-10).
% 42.00000347509558


