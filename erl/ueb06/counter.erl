-module(counter).
-export([start/0, loop/1]).

loop(Count) ->
    io:format("Counter ~p: ~p~n", [self(), Count]),
    receive
        reset -> loop(0);
        up -> loop(Count+1);
        down -> loop(Count-1)
    end.

start() -> spawn(?MODULE, loop, [0]).

