-module(counter_malte).
-export([loop/1, start/0, get/1, set/2, broadcast/2, echo/1, spawn_echo/1]).

loop(C) ->
   receive
       reset -> loop(0);
       up -> loop(C+1);
       down -> if C == 0 -> exit(underflow); true -> loop(C-1) end;
       {get, Who, Tag} -> Who ! {counter_value, Tag, C}, loop(C);
       {set, Value} -> loop(Value);
       show -> io:format("Counter: ~w~n", [C]), loop(C);
       stop -> io:format("~w is going down.~n",[self()]), exit(normal);
       Unknown -> io:format("Received unknown message: ~p", [Unknown]), loop(C)
   end.

start() -> spawn(?MODULE, loop, [0]).

broadcast([], _) -> [];
broadcast([H|T], Msg) ->
   H!Msg,
   broadcast(T, Msg).

get(Pid) ->
    Tag = erlang:now(),
    Pid ! {get, self(), Tag},
    receive
       {counter_value, Tag, C} -> C
       after 10000 -> timeout
    end.

set(Pid, Value) ->
    Pid ! {set, Value} .

echo(Pid) ->
    receive
       Echo -> io:format("~p", [Echo]),
               Pid ! Echo,
               echo(Pid)
    end.

spawn_echo(Pid) ->
    link(Pid),
    spawn(?MODULE, echo, [Pid]).
